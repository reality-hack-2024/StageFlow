# Stageflow

Stageflow is an application designed to give players an opportunity to experience what it's like to perform in front of an audience. Currently, you play at a set of drums in a large concert hall.

## Setup

Ensure that the Unity Game Engine is installed. If you want to modify the project, ensure that a C# code editor is installed such as Visual Studio.

### Hardware Required

- A Meta Quest 3 and its controllers

### Software Dependencies

- Unity Game Engine version `2022.3.18f1` or greater

## Run

1. Open the project in Unity
2. Click `File > Build Settings...`
3. Ensure the desired platform is selected
4. Click `Build` or `Build and Run`

## Shout-Outs

Shout out to MIT for hosting this event and dealing with our presence!
