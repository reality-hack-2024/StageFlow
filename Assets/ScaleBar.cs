using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleBar : MonoBehaviour
{
    public Transform barTransform; // Assign the bar's transform or its parent if you've adjusted the pivot
    public float scaleAmount = 1.0f; // The desired scale amount

    void Update()
    {
        scaleAmount = Mathf.Clamp(scaleAmount, 0, 1);
        // This will scale the bar along its local X axis
        barTransform.localScale = new Vector3(barTransform.localScale.x, barTransform.localScale.y, Mathf.Clamp(1 - scaleAmount, 0, 1));
    }

    // Call this method to set the scale based on some input
    public void SetScale(float newScaleAmount)
    {
        // You can add checks here to ensure newScaleAmount is within desired bounds
        scaleAmount = newScaleAmount;
    }
}
