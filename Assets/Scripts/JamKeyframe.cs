using System;

using Musical.Trumpet;

namespace Musical
{
    /// <summary>
    /// A "keyframe" within a <see cref="Jam"/>. This specifies what notes should be played at what time within a track.
    /// </summary>
    [Serializable]
    public struct JamKeyframe<T> where T : unmanaged, Enum
    {
        /// <summary>The time (in seconds) in which this keyframe occurs.</summary>
        public double Time;
        /// <summary>The instrument state within the current frame.</summary>
        public T State;
    }
}