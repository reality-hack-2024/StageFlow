using UnityEngine;

public class CapsulePropertyUpdater : MonoBehaviour
{
    public Color newBaseColor = Color.red; // The new base color you want to apply
    public float newSaturation = 1.0f; // The new saturation value you want to apply

    /* void Update()
    {
        // For testing purposes, let's call the update method every frame
        // In production, you'd want to call this in response to a specific event
        UpdateCapsuleProperties();
    } */

    // This method updates the material properties of the capsules
    public void UpdateCapsuleProperties()
    {
        // Find all capsules with the tag "CapsuleTag"
        GameObject[] capsules = GameObject.FindGameObjectsWithTag("Capsules");
        foreach (GameObject capsule in capsules)
        {
            // Access the Renderer component and modify the material properties
            Renderer capsuleRenderer = capsule.GetComponent<Renderer>();
            if (capsuleRenderer != null)
            {
                MaterialPropertyBlock props = new MaterialPropertyBlock();
                capsuleRenderer.GetPropertyBlock(props);

                // Set the base color and saturation
                props.SetColor("_Base_Color", newBaseColor);
                props.SetFloat("_Saturation", newSaturation);

                capsuleRenderer.SetPropertyBlock(props);

                Debug.Log("Updated base color and saturation for capsule: " + capsule.name);
            }
            else
            {
                Debug.LogError("Renderer not found on capsule: " + capsule.name);
            }
        }
    }
}
