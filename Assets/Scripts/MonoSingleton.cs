using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Musical
{
    /// <summary>
    /// A singleton <see cref="MonoBehaviour"/> instance. The instance is lazily loaded and created whenever the type is first referenced.
    /// </summary>
    /// <typeparam name="T0">The singleton type.</typeparam>
    public abstract class MonoSingleton<T0> : MonoBehaviour where T0 : MonoSingleton<T0>
    {
#if ENABLE_MONO
        private readonly static GameObject _object;
        private readonly static T0 _instance;

        private static string ObjectName => $"{typeof(T0).Name}_Singleton";

        static MonoSingleton()
        {
            if (IsValid)
                return;

            if (_object = GameObject.Find(ObjectName))
            {
                _instance = _object.GetComponent<T0>();
                return;
            }

            _object = new GameObject(ObjectName);
            _instance = _object.AddComponent<T0>();
            DontDestroyOnLoad(_object);
        }
#else
        private static GameObject _object;
        private static T0 _instance;

        protected internal static void __INITIALIZE()
        {
            Debug.Log($"Create {typeof(T0).Name}!!");
            _object = new GameObject();
            _instance = _object.AddComponent<T0>();
            DontDestroyOnLoad(_object);
        }
#endif

        /* Commented out because this should never be called-- it's bad design
        /// <summary>Destroys the current existing singleton instance.</summary>
        public static void Destroy()
        {
            Destroy(_object);
        }
        */

        /// <summary>Gets the singleton <see cref="T0"/> instance.</summary>
        public static T0 Instance => _instance;
        /// <summary>Gets the singleton <see cref="GameObject"/> which the instance is attached to.</summary>
        public static GameObject Object => _object;

        /// <summary>
        /// Gets if the singleton has a valid instance. Returns <see langword="true"/> if the instance is valid, otherwise <see langword="false"/>.
        /// </summary>
        public static bool IsValid => _instance;
    }
}
