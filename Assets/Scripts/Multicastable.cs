using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Musical
{
    public readonly struct Multicastable<T0, T1>
        where T0 : class
        where T1 : class
    {
        private readonly object _value;

        public static implicit operator Multicastable<T0, T1>(T0 value)
            => new Multicastable<T0, T1>(value);
        public static implicit operator Multicastable<T0, T1>(T1 value)
            => new Multicastable<T0, T1>(value);

        public readonly T0 Value0 => _value as T0;
        public readonly T1 Value1 => _value as T1;

        public Multicastable(T0 value)
            => _value = value;
        public Multicastable(T1 value)
            => _value = value;
    }

    public readonly struct Multicastable<T0, T1, T2>
        where T0 : class
        where T1 : class
        where T2 : class
    {
        private readonly object _value;

        public static readonly Multicastable<T0, T1, T2> Null = new Multicastable<T0, T1, T2>((T0)null);

        public static implicit operator Multicastable<T0, T1, T2>(T0 value)
            => new Multicastable<T0, T1, T2>(value);
        public static implicit operator Multicastable<T0, T1, T2>(T1 value)
            => new Multicastable<T0, T1, T2>(value);
        public static implicit operator Multicastable<T0, T1, T2>(T2 value)
            => new Multicastable<T0, T1, T2>(value);

        public readonly T0 Value0 => _value as T0;
        public readonly T1 Value1 => _value as T1;
        public readonly T2 Value2 => _value as T2;

        public Multicastable(T0 value)
            => _value = value;
        public Multicastable(T1 value)
            => _value = value;
        public Multicastable(T2 value)
            => _value = value;
    }
}
