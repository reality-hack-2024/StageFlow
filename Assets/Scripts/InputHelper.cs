using Oculus.Interaction.Input;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

using Hand = Oculus.Interaction.Input.Hand;

namespace Musical
{
    /// <summary>
    /// Input helper to help manage XR simultaneous hand and controller tracking.
    /// </summary>
    public class InputHelper : MonoSingleton<InputHelper>
    {
        private InputDevice _leftDevice;
        private InputDevice _rightDevice;
        private InputDevice _headDevice;

#if !ENABLE_MONO
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void InitHelper()
        {
            InputHelper.__INITIALIZE();
        }
#endif

        private void RefreshInput()
        {
            List<InputDevice> devices = new List<InputDevice>(1);
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Left, devices);

            if (devices.Count != 0)
            {
                _leftDevice = devices[0];
            }
            else
                _leftDevice = new InputDevice(); // Invalid value -- maybe hide controller or show popup in this case

            devices.Clear();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right, devices);

            if (devices.Count != 0)
            {
                _rightDevice = devices[0];
            }
            else
                _rightDevice = new InputDevice(); // Invalid value -- maybe hide controller or show popup in this case


            devices.Clear();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.HeadMounted, devices);

            if (devices.Count != 0)
            {
                _headDevice = devices[0];
            }
            else
                _headDevice = new InputDevice(); // Invalid value -- maybe hide controller or show popup in this case
        }

        private void Awake()
        {
            InputDevices.deviceConnected += _DeviceListChanged;
            InputDevices.deviceDisconnected += _DeviceListChanged;

            RefreshInput();
        }

        private void OnDestroy()
        {
            InputDevices.deviceConnected -= _DeviceListChanged;
            InputDevices.deviceDisconnected -= _DeviceListChanged;
        }

        private void _DeviceListChanged(InputDevice obj)
            => RefreshInput();
        private static readonly Quaternion OculusQuestRotationFix = Quaternion.Euler(45.0f, 0.0f, 0.0f);
        private static Quaternion AdjustDeviceRotation(Quaternion quat)
            => quat * OculusQuestRotationFix;

        /// <summary>
        /// Gets the position of the left hand/controller.
        /// </summary>
        public static Vector3 LeftPosition
            => Instance._leftDevice.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 value) ? value : Vector3.zero;
        /// <summary>
        /// Gets the position of the right hand/controller.
        /// </summary>
        public static Vector3 RightPosition
            => Instance._rightDevice.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 value) ? value : Vector3.zero;
        /// <summary>
        /// Gets the rotation of the left hand/controller.
        /// </summary>
        public static Quaternion LeftRotation
            => Instance._leftDevice.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion value)
                ? AdjustDeviceRotation(value) : OculusQuestRotationFix;
        /// <summary>
        /// Gets the rotation of the right hand/controller.
        /// </summary>
        public static Quaternion  RightRotation
            => Instance._rightDevice.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion value)
                ? AdjustDeviceRotation(value) : OculusQuestRotationFix;
        /// <summary>
        /// Gets the velocity of the left hand/controller.
        /// </summary>
        public static Vector3 LeftVelocity
            => Instance._leftDevice.TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 value) ? value : Vector3.zero;
        /// <summary>
        /// Gets the velocity of the right hand/controller.
        /// </summary>
        public static Vector3 RightVelocity
            => Instance._rightDevice.TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 value) ? value : Vector3.zero;
        /// <summary>
        /// Gets the velocity of the left hand/controller.
        /// </summary>
        public static Vector3 LeftAngularVelocity
            => Instance._leftDevice.TryGetFeatureValue(CommonUsages.deviceAngularVelocity, out Vector3 value) ? value : Vector3.zero;
        /// <summary>
        /// Gets the velocity of the right hand/controller.
        /// </summary>
        public static Vector3 RightAngularVelocity
            => Instance._rightDevice.TryGetFeatureValue(CommonUsages.deviceAngularVelocity, out Vector3 value) ? value : Vector3.zero;

        /// <summary>Gets if the left input method is a <see cref="Hand"/>.</summary>
        public static bool IsLeftHand => Instance._leftDevice.characteristics.FastHasFlag(InputDeviceCharacteristics.HandTracking);
        /// <summary>Gets if the right input method is a <see cref="Hand"/>.</summary>
        public static bool IsRightHand => Instance._rightDevice.characteristics.FastHasFlag(InputDeviceCharacteristics.HandTracking);

        /// <summary>Gets if the left input method exists and is valid.</summary>
        public static bool HasLeft => Instance._leftDevice.isValid;
        /// <summary>Gets if the right input method exists and is valid.</summary>
        public static bool HasRight => Instance._rightDevice.isValid;
        /// <summary>Gets if the headset device exists and is valid.</summary>
        public static bool HasHead => Instance._headDevice.isValid;
    }
}