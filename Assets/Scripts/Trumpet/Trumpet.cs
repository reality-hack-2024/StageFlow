using Musical.Drums;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Musical.Trumpet
{
    [AddComponentMenu("Musical/Trumpet")]
    public class Trumpet : MonoBehaviour
    {
        [SerializeField]
        private TrumpetValve[] _valves;
        private ValveState _state;

#if DEBUG
        [SerializeField]
        private Key[] _debugKeys; // Used on keyboard to test input response
#endif

        /// <summary>
        /// Bitflags which describe the state of the valves.
        /// </summary>
        public ValveState ValveFlags => _state;

        /// <summary>
        /// Connects to the events needed to function.
        /// </summary>
        private void Awake()
        {
            foreach (TrumpetValve v in _valves)
            {
                v.OnPressed += ValvePressed;
                v.OnReleased += ValveReleased;
            }

#if DEBUG
            InputSystem.onAfterUpdate += _DebugInputUpdate;
#endif
        }

#if DEBUG
        private void _DebugInputUpdate()
        {
            for (int i = 0; i < _valves.Length; ++i)
            {
                if (Keyboard.current[_debugKeys[i]].wasPressedThisFrame)
                    ValvePressed((ValveState)(1 << i));
                else if (Keyboard.current[_debugKeys[i]].wasReleasedThisFrame)
                    ValveReleased((ValveState)(1 << i));
            }
        }
#endif

        /// <summary>
        /// Disconnects from the events added in <see cref="Awake"/>
        /// </summary>
        private void OnDestroy()
        {
#if DEBUG
            InputSystem.onAfterUpdate -= _DebugInputUpdate;
#endif

            foreach (TrumpetValve v in _valves)
            {
                v.OnPressed -= ValvePressed;
                v.OnReleased -= ValveReleased;
            }
        }

        /// <summary>
        /// Called when a <see cref="TrumpetValve"/> is pressed.
        /// </summary>
        /// <param name="state">The flag corresponding to the pressed valve.</param>
        private void ValvePressed(ValveState state)
        {
            _state |= state;

            // TODO: Timing/judgement code can go here...
        }

        /// <summary>
        /// Called when a <see cref="TrumpetValve"/> is released.
        /// </summary>
        /// <param name="state">The flag corresponding to the released valve.</param>
        private void ValveReleased(ValveState state)
        {
            _state ^= state;

            // TODO: Timing/judgement code can go here...
        }

        // Shouldn't *NEED* to implement `.Update()` here, but we'll see...
    }
}