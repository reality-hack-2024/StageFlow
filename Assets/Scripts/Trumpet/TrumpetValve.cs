using UnityEngine;

namespace Musical.Trumpet
{
    [AddComponentMenu("Musical/TrumpetValve")]
    public class TrumpetValve : MonoBehaviour
    {
        [SerializeField]
        private ValveState _valve;

        public delegate void ValveHandler(ValveState valve);

        // These events are expected to never be null if they are properly connected to a Trumpet object.
        public event ValveHandler OnPressed;
        public event ValveHandler OnReleased;

        public void Press()
        {
            OnPressed(_valve);
        }

        public void Release()
        {
            OnReleased(_valve);
        }
    }
}