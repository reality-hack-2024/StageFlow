using System;
using UnityEngine;

namespace Musical.Trumpet
{
    [Flags]
    public enum ValveState
    {
        /// <summary>First trumpet valve.</summary>
        [InspectorName("First Valve")]
        First = 1,
        /// <summary>Second trumpet valve.</summary>
        [InspectorName("Second Valve")]
        Second = 2,
        /// <summary>Third trumpet valve.</summary>
        [InspectorName("Third Valve")]
        Third = 4,
    }

    public static class ValveStateExtensions
    {
        /// <summary>Gets the state of the first valve.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="ValveState.First"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsFirstPressed(this ValveState state) { return (state & ValveState.First) != 0; }
        /// <summary>Gets the state of the second valve.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="ValveState.Second"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsSecondPressed(this ValveState state) { return (state & ValveState.Second) != 0; }
        /// <summary>Gets the state of the third valve.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="ValveState.Third"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsThirdPressed(this ValveState state) { return (state & ValveState.Third) != 0; }
    }
}
