using Oculus.Interaction.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Musical.Debugging
{
    public class HandSphere : MonoBehaviour
    {
        [SerializeField]
        private Handedness _hand;

        private void Update()
        {
            switch (_hand)
            {
                case Handedness.Left:
                    transform.SetPositionAndRotation(InputHelper.LeftPosition, InputHelper.LeftRotation);
                    break;
                case Handedness.Right:
                    transform.SetPositionAndRotation(InputHelper.RightPosition, InputHelper.RightRotation);
                    break;
            }
        }
    }
}
