using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Musical
{
    public class Player : MonoBehaviour
    {
        private int score = 0;
        private float flowState = 0; // Example flow state variable

        // TODO: make these params configurable in unity?
        private const float FlowIncreasePerHit = 0.1f; // Increase in flow state per hit
        private const float FlowDecreaseOverTime = 0.01f; // Decrease in flow state over time

        private void Update()
        {
            // Decrease flow state over time
            if (flowState > 0)
            {
                Debug.Log($"Decreasing flow state to {flowState}\t\tin Player.cs::Update()");
                flowState -= FlowDecreaseOverTime * Time.deltaTime;
            }
        }

        public void UpdateScore(int scoreToAdd)
        {
            score += scoreToAdd;

            // Update flow state based on the score of the hit, ranging from 0-1 max
            // TODO: different increases based on the score
            flowState += FlowIncreasePerHit;
            flowState = Mathf.Clamp(flowState, 0, 1);
            Debug.Log($"Updated Player Score: {score}, Flow State: {flowState}\t\t in Player.cs::UpdateScore()");

            // TODO: Update the UI
        }
    }
}
