using UnityEngine;

public class CapsuleGridInstantiator : MonoBehaviour
{
    public GameObject capsulePrefab;
    public int rows = 10;
    public int columns = 10;
    public float spacing = 1.0f;
    public GameObject parentObject; // Parent GameObject
    public string capsuleTag = "Capsules";

    void Start()
    {
        // Create the parent object if not assigned
        if (parentObject == null)
        {
            parentObject = new GameObject("CapsuleGridParent");
        }

        // Set the parent object's position (optional, can be (0,0,0) or a specific point)
        parentObject.transform.position = new Vector3(-60.0f, -2.0f, -45.0f); // or any other specific point

        // Instantiate capsules at global positions
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                Vector3 globalPosition = new Vector3(i * spacing, 0, j * spacing);
                GameObject capsule = Instantiate(capsulePrefab, globalPosition, Quaternion.identity);
                capsule.transform.SetParent(parentObject.transform, false);
                capsule.tag = capsuleTag;
            }
        }

        // Rotate the parent object to slant the entire grid
        parentObject.transform.rotation = Quaternion.Euler(0, 0, 5.0f);
    }
}
