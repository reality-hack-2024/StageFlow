using Musical.Drums;
using Musical.Trumpet;

using UnityEngine;

namespace Musical
{
    public struct TimingWindow
    {
        public float PerfectHit; // e.g., �20ms
        public float GoodHit;    // e.g., �40ms
        public float OkHit;      // e.g., �70ms
                                 // can add more segments as needed

        public TimingWindow(float perfect, float good, float ok)
        {
            PerfectHit = perfect;
            GoodHit = good;
            OkHit = ok;
        }
    }

    [CreateAssetMenu(fileName = "New Jam", menuName = "Musical/Create New Jam")]
    public class Jam : ScriptableObject
    {
        [SerializeField]
        private string _name;
        [SerializeField]
        private AudioClip _music;
        [SerializeField]
        private JamKeyframe<DrumState>[] _drumFrames;
        [SerializeField]
        private JamKeyframe<ValveState>[] _trumpetFrames;

        public string Name => _name;
        public AudioClip Music => _music;

        public int DrumKeyframeCount => _drumFrames.Length;
        public JamKeyframe<DrumState> GetDrumKeyframe(int index) => _drumFrames[index];

        public int TrumpetKeyframeCount => _trumpetFrames.Length;
        public JamKeyframe<ValveState> GetTrumpetKeyframe(int index) => _trumpetFrames[index];

        // Method to get all drum keyframes
        public JamKeyframe<DrumState>[] GetDrumKeyframes()
        {
            return _drumFrames;
        }

    }
}