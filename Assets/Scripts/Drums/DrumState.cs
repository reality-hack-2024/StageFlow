using System;
using UnityEngine;

namespace Musical.Drums
{
    [Flags]
    public enum DrumState
    {
        /// <summary>Snare drum.</summary>
        [InspectorName("Snare Drum")]
        Snare = 1,
        /// <summary>High tom.</summary>
        [InspectorName("High Tom")]
        HiTom = 2,
        /// <summary>Low tom.</summary>
        [InspectorName("Low Tom")]
        LoTom = 4,
        /// <summary>Cymbal.</summary>
        [InspectorName("Floor Tom")]
        FloorTom = 8,
    }

    public static class DrumStateExtensions
    {
        /// <summary>Gets the state of the snare drum.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="DrumState.Snare"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsSnareHit(this DrumState state) { return (state & DrumState.Snare) != 0; }
        /// <summary>Gets the state of the high tom.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="DrumState.HiTom"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsHiTomHit(this DrumState state) { return (state & DrumState.HiTom) != 0; }
        /// <summary>Gets the state of the low tom.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="DrumState.LoTom"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsLowTomHit(this DrumState state) { return (state & DrumState.LoTom) != 0; }
        /// <summary>Gets the state of the cymbal.</summary>
        /// <param name="state">The current state.</param>
        /// <returns>Returns <see langword="true"/> if the flag <see cref="DrumState.Cymbal"/> is set. Otherwise, <see langword="false"/>.</returns>
        public static bool IsFloorTomHit(this DrumState state) { return (state & DrumState.FloorTom) != 0; }
    }
}
