using Oculus.Interaction.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Musical.Drums
{
    [AddComponentMenu("Musical/Drum Kit")]
    public class Drums : MonoBehaviour
    {
        [SerializeField]
        private SingleDrum[] _drums;
        private DrumState _state;


        // Reference to the current Jam session
        [SerializeField]
        private Jam currentJam;

        // Timing window for scoring
        private TimingWindow timingWindow = new TimingWindow(0.04f, 0.08f, 0.12f); // Example values

        // Reference to the player for score updates
        [SerializeField]
        private Player player;

        /// <summary>
        /// Bitflags which describe the state of the drums.
        /// </summary>
        public DrumState State => _state;

        /// <summary>
        /// Connects to the events needed to function.
        /// </summary>
        private void Awake()
        {
            // Ensure _drums array is properly initialized
            // _drums is never null because the Unity always gives it a valid reference
            if (_drums.Length == 0)
            {
                Debug.LogError("Drums array is not initialized or empty.\t\t in Drums.cs::Awake()");
                return;
            }

            foreach (SingleDrum v in _drums)
            {
                v.OnHit += DrumHit;
                v.OnLift += DrumLift;
            }

        }

        /// <summary>
        /// Disconnects from the events added in <see cref="Awake"/>
        /// </summary>
        private void OnDestroy()
        {
            foreach (SingleDrum v in _drums)
            {
                v.OnHit -= DrumHit;
                v.OnLift -= DrumLift;
            }
        }

        /// <summary>
        /// Called when a <see cref="SingleDrum"/> is hit.
        /// </summary>
        /// <param name="state">The flag corresponding to the pressed valve.</param>
        private void DrumHit(DrumState state, Handedness hand)
        {
            _state |= state;

            float currentTime = Time.time; // Or use the audio playback time
            float closestTimeDiff = GetClosestTimeDifference(currentTime);

            int score = CalculateScore(closestTimeDiff);
            Debug.Log($"Drum Hit: {state}, Timing Difference: {closestTimeDiff}, Score: {score}\t\t in Drums.cs::DrumHit()");
            // Null check in case something goes wrong
            player?.UpdateScore(score); // Update the player's score
        }

        private float GetClosestTimeDifference(float currentTime)
        {
            // Logic to find the closest keyframe time to currentTime and return the time difference
            // Placeholder logic, needs to be implemented based on how JamKeyframes are structured

            if (currentJam == null)
                return 0.0f; // This shoould theoretically never return if we did everything right...

            float minDiff = float.MaxValue;
            foreach (var frame in currentJam.GetDrumKeyframes())
            {
                float diff = Mathf.Abs((float)frame.Time - currentTime);
                if (diff < minDiff)
                {
                    minDiff = diff;
                }
            }
            return minDiff;
        }

        private int CalculateScore(float timeDiff)
        {
            // Use TimingWindow to calculate the score based on timeDiff
            // Example scoring logic
            if (timeDiff <= timingWindow.PerfectHit) return 100;
            if (timeDiff <= timingWindow.GoodHit) return 75;
            if (timeDiff <= timingWindow.OkHit) return 50;
            return 0;
        }

        /// <summary>
        /// Called when a stick is lifted from the <see cref="SingleDrum"/>.
        /// </summary>
        /// <param name="state">The flag corresponding to the pressed valve.</param>
        private void DrumLift(DrumState state, Handedness hand)
        {
            _state ^= state;
        }

        // Shouldn't *NEED* to implement `.Update()` here, but we'll see...
    }
}