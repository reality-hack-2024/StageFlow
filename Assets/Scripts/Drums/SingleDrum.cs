using Musical.Trumpet;
using Oculus.Interaction.Input;
using Unity.XR.Oculus.Input;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace Musical.Drums
{
    [AddComponentMenu("Musical/Drum")]
    [RequireComponent(typeof(AudioSource))]
    public class SingleDrum : MonoBehaviour
    {
        [SerializeField]
        private DrumState _drum;
        [SerializeField]
        private AudioClip _hardHit;
        [SerializeField]
        private AudioClip _softHit;
        private AudioSource _source;

        private bool _left;
        private bool _right;

#if DEBUG
        [SerializeField]
        private Key _debugKey;
#endif

        private void Awake()
        {
            _source = GetComponent<AudioSource>();

#if DEBUG
            InputSystem.onAfterUpdate += _DebugInputUpdate;
#endif
        }

#if DEBUG
        private void _DebugInputUpdate()
        {
            if (Keyboard.current[_debugKey].wasPressedThisFrame)
                Hit(Handedness.Left);
            else if (Keyboard.current[_debugKey].wasReleasedThisFrame)
                Lift(Handedness.Left);
        }
#endif

        public delegate void DrumHandler(DrumState valve, Handedness hand);

        // These events are expected to never be null if they are properly connected to a Drums object.
        public event DrumHandler OnHit;
        public event DrumHandler OnLift;

        public void Hit(Handedness hand)
        {
            switch (hand)
            {
                case Handedness.Left:
                    if (_left)
                        return;
                    _left = true;
                    break;
                case Handedness.Right:
                    if (_right)
                        return;
                    _right = true;
                    break;
            }

            OnHit(_drum, hand);

            _source.clip = _hardHit;
            _source.Play();
        }
        public void Lift(Handedness hand)
        {
            switch (hand)
            {
                case Handedness.Left:
                    if (!_left)
                        return;
                    _left = false;
                    break;
                case Handedness.Right:
                    if (!_right)
                        return;
                    _right = false;
                    break;
            }

            OnLift(_drum, hand);
        }
    }
}