using Oculus.Interaction.Input;
using UnityEngine;
using System.Linq;
using Oculus.Interaction.Body.Input;
using Oculus.Interaction;

namespace Musical.Drums
{
    public class Stick : MonoBehaviour
    {
        //public LayerMask layer;
        //private Vector3 previousPos;

        [SerializeField]
        private Handedness _hand;
        [SerializeField]
        private CapsuleCollider _collider;
        [SerializeField]
        private Rigidbody _body;
        [SerializeField]
        private Transform _visual;
        [SerializeField]
        private Vector3 _angleOffset;
        [SerializeField]
        private Vector3 _posOffset;
        [SerializeField]
        private Vector3 _scaling;

        private Quaternion _rotOffset;
        private SingleDrum _current;

        private void Awake()
        {
            _rotOffset = Quaternion.Euler(_angleOffset);
        }

        private void Start()
        {
            switch (_hand)
            {
                case Handedness.Left:
                    _body.transform.localPosition = Vector3.Scale(InputHelper.LeftPosition + _posOffset, _scaling);
                    _body.transform.localRotation = InputHelper.LeftRotation * _rotOffset;
                    break;
                case Handedness.Right:
                    _body.transform.localPosition = Vector3.Scale(InputHelper.RightPosition + _posOffset, _scaling);
                    _body.transform.localRotation = InputHelper.RightRotation * _rotOffset;
                    break;
            }
        }

        public void OnHit(Collision collision)
        {
            if (collision.gameObject.CompareTag("Drum") && collision.gameObject.TryGetComponent(out SingleDrum dr))
            {
                _current = dr;
                _current.Hit(_hand);
            }
        }
        public void OnLift(Collision collision)
        {
            if (collision.gameObject.CompareTag("Drum") && collision.gameObject.TryGetComponent(out SingleDrum dr) && dr == _current)
            {
                _current.Lift(_hand);
                _current = null;
            }
        }

        private const float DAMP = 1.0f;
        private const float FREQ = 30.0f;
        private const float KP = (6.0f * FREQ) * (6.0f * FREQ) * 0.25f;
        private const float KD = 4.5f * ANGFREQ * DAMP;
        private const float ANGFREQ = 40.0f;
        private const float ANGKP = (6.0f * ANGFREQ) * (6.0f * ANGFREQ) * 0.25f;
        private const float ANGKD = 4.5f * ANGFREQ * DAMP;

        private float SpdPosAccelComponent(float from, float to, float fromVelocity, float toVelocity)
        {
            return -KP * (from + Time.fixedDeltaTime * fromVelocity - to + Time.fixedDeltaTime * toVelocity) - KD * (fromVelocity - toVelocity);
        }

        private Vector3 SpdPosAccel(Vector3 from, Vector3 to, Vector3 fromVelocity, Vector3 toVelocity)
            => new Vector3(
                SpdPosAccelComponent(from.x, to.x, fromVelocity.x, toVelocity.x),
                SpdPosAccelComponent(from.y, to.y, fromVelocity.y, toVelocity.y),
                SpdPosAccelComponent(from.z, to.z, fromVelocity.z, toVelocity.z));

        private Vector3 SpdForce(Vector3 from, Vector3 to, Vector3 fromVelocity, Vector3 toVelocity)
            => SpdPosAccel(from, to, fromVelocity, toVelocity);

        private Vector3 SpdRotateAccel(Quaternion from, Quaternion to, Vector3 fromVelocity, Vector3 toVelocity)
        {
            Quaternion rt;
            float omt = toVelocity.magnitude;
            if (omt < 10e-8f)
                rt = to;
            else
                rt = Quaternion.AngleAxis(omt * Time.fixedDeltaTime * Mathf.Rad2Deg, toVelocity / omt) * to;

            Quaternion r;
            float om = toVelocity.magnitude;
            if (om < 10e-8f)
                r = from;
            else
                r = Quaternion.AngleAxis(om * Time.deltaTime * Mathf.Rad2Deg, fromVelocity / om) * from;

            Quaternion dp = r * Quaternion.Inverse(rt);

            if (dp.w < 0.0f)
            {
                dp.x = -dp.x;
                dp.y = -dp.y;
                dp.z = -dp.z;
                dp.w = -dp.w;
            }

            dp.ToAngleAxis(out float dm, out Vector3 dd);
            dd.Normalize();
            dm *= Mathf.Deg2Rad;

            return dm * -ANGKP * dd - ANGKD * (fromVelocity - toVelocity);
        }

        private Vector3 SpdTorque(Quaternion from, Quaternion to, Vector3 fromVelocity, Vector3 toVelocity)
        {
            Vector3 tPrin = _body.inertiaTensor;
            tPrin.Scale(Quaternion.Inverse(from) * SpdRotateAccel(from, to, fromVelocity, toVelocity));
            return from * _body.inertiaTensorRotation * tPrin;
        }

        private void FixedUpdate()
        {
            //Vector3 targetPos;
            //Vector3 targetVel;
            //Quaternion targetRot;
            //Vector3 targetAngVel;
            //Debug.Log($"Left controller pos: {InputHelper.LeftPosition}");
            //switch (_hand)
            //{
            //    case Handedness.Left:
            //        targetPos = transform.parent.position + Vector3.Scale(InputHelper.LeftPosition + _posOffset, _scaling);
            //        targetVel = InputHelper.LeftVelocity;
            //        targetRot = transform.parent.rotation * InputHelper.LeftRotation * _rotOffset;
            //        targetAngVel = InputHelper.LeftAngularVelocity;
            //        break;
            //    case Handedness.Right:
            //        targetPos = transform.parent.position + Vector3.Scale(InputHelper.RightPosition + _posOffset, _scaling);
            //        targetVel = InputHelper.RightVelocity;
            //        targetRot = transform.parent.rotation * InputHelper.RightRotation * _rotOffset;
            //        targetAngVel = InputHelper.RightAngularVelocity;
            //        break;
            //    default:
            //        // how
            //        targetPos = transform.parent.position + _posOffset;
            //        targetVel = Vector3.zero;
            //        targetRot = transform.parent.rotation * _rotOffset;
            //        targetAngVel = Vector3.zero;
            //        break;
            //}

            //// NOTE: This method doesn't snap very well to the visual, but it's a good temporary solution.
            ////       I may be able to tweak this later to work better when switching directions. -Jaiden
            //_body.AddForce(SpdForce(_body.position, targetPos, _body.velocity, targetVel), ForceMode.Acceleration);
            //_body.AddTorque(SpdTorque(_body.rotation, targetRot, _body.angularVelocity, targetAngVel), ForceMode.Acceleration);
        }

        private void Update()
        {
            //_visual.SetPositionAndRotation(_body.position, _body.rotation);
        }
    }
}