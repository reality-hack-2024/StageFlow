using Oculus.Interaction.Input;
using UnityEngine;
using System.Linq;
using Oculus.Interaction.Body.Input;
using Oculus.Interaction;

namespace Musical.Drums
{
    public class StickSignaler : MonoBehaviour
    {
        [SerializeField]
        private Stick _stick;

        public void OnCollisionEnter(Collision collision)
            => _stick.OnHit(collision);
        public void OnCollisionExit(Collision collision)
            => _stick.OnLift(collision);
    }
}