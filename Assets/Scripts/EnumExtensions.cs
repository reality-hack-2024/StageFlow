namespace Musical
{
    public static class EnumExtensions
    {
        public static unsafe bool FastHasFlag<T>(this T value, T flag) where T : unmanaged, System.Enum
        {
            switch (sizeof(T))
            {
                case 1:
                    return (*(byte*)&value & *(byte*)&flag) == *(byte*)&flag;
                case 2:
                    return (*(ushort*)&value & *(ushort*)&flag) == *(ushort*)&flag;
                case 4:
                    return (*(uint*)&value & *(uint*)&flag) == *(uint*)&flag;
                case 8:
                    return (*(ulong*)&value & *(ulong*)&flag) == *(ulong*)&flag;
                default:
                    throw new System.InvalidOperationException("Invalid enum size"); // I'm not dealing with the slow branch -Jaiden
            }
        }
    }
}